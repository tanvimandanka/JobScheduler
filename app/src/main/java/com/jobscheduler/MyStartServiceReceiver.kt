package com.jobscheduler

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

/**
 * Created by tanvi on 27/2/18.
 */
class MyStartServiceReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context, p1: Intent?) {
        Util.scheduleJob(p0)
    }
}