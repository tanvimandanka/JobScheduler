package com.jobscheduler

import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.widget.Toast

/**
 * Created by tanvi on 27/2/18.
 */
class TestJobService : JobService() {
    //when device connected to chargeing onStartJob will be triggered
    override fun onStartJob(p0: JobParameters?): Boolean {
        Toast.makeText(applicationContext, "onJobRun", Toast.LENGTH_SHORT).show()
        return false
    }

    override fun onStopJob(p0: JobParameters?): Boolean {
        return false
    }
}