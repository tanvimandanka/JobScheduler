package com.jobscheduler

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context

class Util {

    companion object {

        /**
         * schedule job for when device connected to charger
         */
        fun scheduleJob(context: Context) {
            val serviceComponent = ComponentName(context, TestJobService::class.java)
            val builder = JobInfo.Builder(0, serviceComponent)
            builder.setMinimumLatency(1 * 1000)
            builder.setOverrideDeadline(3 * 1000)
            builder.setRequiresCharging(true)

            val jobScheduler = context.getSystemService(JobScheduler::class.java)
            jobScheduler.schedule(builder.build())

        }
    }
}

